<script  type="text/javascript" src="<?php echo base_url()?>basicprimitives/jquery-1.9.1.js"></script>

<script  type="text/javascript" src="<?php echo base_url()?>basicprimitives/jquery-ui-1.10.2.custom.min.js"></script>
<script  type="text/javascript" src="<?php echo base_url()?>basicprimitives/primitives.min.js?219"></script>

<link href="<?php echo base_url()?>basicprimitives/primitives.latest.css?219" media="screen" rel="stylesheet" type="text/css" />
Hello, dept number <?php echo '' ?>

<?php
/**
* 
*/
class Personnel
{
	public $id;
	public $idBoss;
	public $names;
	public $desc;
	public $email;
}
?>
<script type='text/javascript'>//<![CDATA[ 
        $(window).load(function () {


            var options = new primitives.orgdiagram.Config();

			var items =[
				<?php foreach ($personnel as $key => $value): ?>
				new primitives.orgdiagram.ItemConfig({
					id: <?php echo $value['ID']?>,
                    parent: <?php echo $value['ID_BOSS']?>,
                    title: <?php echo "\"" . $value['NAMES'] . "\""?>,
                    description: <?php echo "\"" . $value['POSITION'] . "\""?>,
                    image: "<?php echo base_url()?>img/bronco.jpg"
                })
				<?php
				if (isset($personnel[$key+1])) {
					?>
					,
					<?php
				}
				?>
				

				<?php endforeach ?>
			];
			
            options.items = items;
            options.cursorItem = 0;
            options.hasSelectorCheckbox = primitives.common.Enabled.True;

            jQuery("#basicdiagram").orgDiagram(options);
        });//]]>  
</script>

<div id="basicdiagram" style="width: 100%; height: 480px; border-width: 1px;" />