<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Agregar Usuario</h3>
        <div class="box-tools pull-right">
            <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button> -->        
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
         <?php
        $roles = [
            'admin' => 'Administrador',
            'common' => 'Usuario común',
            'janitor' => 'Janitor',
            'anon' => 'Anónimo',
            'mod' => 'Mod'
        ] ?>
        <?php

        $classes = "form-control select2";

        $fields = [
            "username" => ["name"=>"Usuario","type"=>"text", "class"=>$classes, "placeholder" => "Usuario" , 'icon' => '<i class="fa fa-fw fa-user"></i>'],
            "password" => ["name"=>"Contraseña","type"=>"password", "class"=>$classes, "placeholder" => "********" , 'icon' => '<i class="fa fa-fw fa-lock"></i>'],
            "passwordConfirm" => ["name"=>"Confirmar contraseña","type"=>"password", "class"=>$classes, "placeholder" => "********" , 'icon' => '<i class="fa fa-fw fa-lock"></i>'],
            "email" => ["name"=>"Correo Electrónico","type"=>"text", "class"=>$classes, "placeholder" => "Correo" , 'icon' => '<i class="fa fa-fw fa-envelope"></i>'],
            "role" => ["name"=>"Rol","type"=>"dropdown", "class"=>$classes, "placeholder" => "Rol", 'options' => $roles , 'icon' => '<i class="fa fa-fw fa-user-o"></i>'],
            "department" => ["name"=>"Dependencia","type"=>"dropdown", "class"=>$classes, "placeholder" => "Dependencia", 'options' => $roles , 'icon' => '<i class="fa fa-fw fa-sitemap"></i>'],
        ]; ?>
       
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php echo form_open('newarticle'); ?>
                    <?php foreach ($fields as $key => $value): ?>
                        <?php echo form_label($value['icon']. $value['name']); ?><br>
                        <?php switch ($value['type']) {
                            case 'text':
                                    echo form_input(array('id' => $key, 'name' => $key, 'class' => $value['class'], 'placeholder' => $value['placeholder']));
                                break;
                            case 'password':
                                    echo form_password(array('id' => $key, 'name' => $key, 'class' => $value['class'], 'placeholder' => $value['placeholder']));
                                break;
                            case 'dropdown':
                                    echo form_dropdown($key,$value['options'],'common', ['class' => $value['class']]);
                                break;
                            
                            default:
                                # code...
                                break;
                        } ?>
                        <br>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <?php echo form_submit(array('id' => 'submit', 'value' => 'Registrar', 'class' => 'btn btn-primary')); ?>
        <?php echo form_close(); ?>
    </div>
</div>