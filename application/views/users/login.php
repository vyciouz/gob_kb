<body class="hold-transition login-page" style="background-image: url(<?php echo base_url() ?>/img/bg2.jpg); background-size: 100% 100%;">
<div class="login-box">
    <div class="login-logo">
        <a style="color: white;" href="<?php echo base_url()?>"><b>🅱urr</b>itos</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body" style="background-color:rgba(4, 0, 0, 0.6); color: white; border: 1px solid white;">
        <p class="login-box-msg">Bienvenido</p>
        <form action="<?php echo base_url() ?>" method="post">
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Usuario" style="background-color:rgba(4, 0, 0, 0); color: white; border: 1px solid white;">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Contraseña" style="background-color:rgba(4, 0, 0, 0); color: white; border: 1px solid white;" >
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
      
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat" style="background-color: #393; border: 1px solid white;">Entrar</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <a href="#" style="color: #6ad; font-weight: bold;">Olvidé mi contraseña</a><br>
    <a href="register.html" style="color: #6ad; font-weight: bold;" class="text-center">Registrarse</a>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?php echo base_url() ?>/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url() ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url() ?>/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>


</body>