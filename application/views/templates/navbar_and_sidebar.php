<body class="hold-transition skin-green sidebar-mini">
	<div class="wrapper">
		<header class="main-header">
			<!-- Logo -->
			<a href="<?php echo base_url(); ?>" class="logo">
				<!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><b>🅱</b>u</span>
				<!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><b>🅱urr</b>itos</span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>

				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- Messages: style can be found in dropdown.less-->
						<li class="dropdown messages-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-envelope-o"></i>
								<!-- In case of alerrts -->
								<!-- <span class="label label-success">0</span> -->
							</a>
							<ul class="dropdown-menu"> 
							</ul>
						</li>

						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="<?php echo base_url() ?>img/happy_negro.png" class="user-image" alt="User Image">
								<span class="hidden-xs">Santino Lee</span>
							</a>
							<ul class="dropdown-menu">
								<!-- User image -->
								<li class="user-header">
									<img src="<?php echo base_url(); ?>/img/happy_negro.png" class="img-circle" alt="User Image">
									<p>
										Santino Lee
									</p>
								</li>
								<!-- Menu Footer-->
								<li class="user-footer">
									<div class="pull-left">
										<a href="#" class="btn btn-default btn-flat">Profile</a>
									</div>
									<div class="pull-right">
										<a href="#" class="btn btn-default btn-flat">Sign out</a>
									</div>
								</li>
							</ul>
						</li>
						<!-- Control Sidebar Toggle Button -->
						<li>
							<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
						</li>
					</ul>
				</div>
			</nav>
		</header>
		
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="<?php echo base_url()?>img/happy_negro.png" class="img-circle" alt="User Image">
					</div>
					<div class="pull-left info">
						<p>Alexander Pierce</p>
						<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
					</div>
				</div>
				<!-- search form -->
				<form action="#" method="get" class="sidebar-form">
					<div class="input-group">
						<input type="text" name="q" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
							<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
							</button>
						</span>
					</div>
				</form>
				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu" data-widget="tree">
					<li class="header">MAIN NAVIGATION</li>
					<li class=""><a href="index.html"><i class="fa fa-circle-o"></i> ALGO</a></li>
					<li class="header">LABELS</li>

					<?php foreach ($departments as $key => $value): ?>
						<li class="treeview">
							<a href="#">
								<i class="fa fa-dashboard"></i> <span><?php echo $departments[$key]['NAME']  ?></span>
								<span class="pull-right-container">
									<i class="fa fa-angle-left pull-right"></i>
								</span>
							</a>
							<ul class="treeview-menu">
								<li class=""><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
								<li><a href="<?php echo base_url(). 'departments/orgchart/'.$departments[$key]['ID']?>"><i class="fa fa-sitemap"></i> Organigrama</a></li>
								<li><a href="<?php echo base_url(). 'departments/info/'.$departments[$key]['ID']?>"><i class="fa fa-sitemap"></i> Información</a></li>
							</ul>
						</li>
					<?php endforeach ?>


				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>
		<!-- /.sidebar -->

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Main content -->
			<section class="content">
				<section class="content-header">
					<h1>
						<?php echo $title ?>
						<small><?php echo $subtitle ?></small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i> Inicio</a></li>
						<?php /*
						Cycle to iterate trought which pages you are.
						Information for this is fed from the controller.
						*/ ?>
						<?php if (is_array($navnodes)): ?>
							<?php foreach ($navnodes as $key => $value): ?>	
						<li class=""><?php echo $value ?></li>
							<?php endforeach ?>
						<?php endif ?>
						<?php if (!is_array($navnodes)): ?>
							<?php if (strcmp($title, "Inicio")): ?>	
							<li class="active"><?php echo $title ?></li>
							<?php endif ?>
						<?php endif ?>
					</ol>
				</section>
				<section class="content">