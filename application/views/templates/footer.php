		</section>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->

	<footer class="main-footer">
		<div class="pull-right hidden-xs">
			<b>Version</b> 2.4.0
		</div>
		<strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
		reserved.
	</footer>


	<!-- Control Sidebar -->
	<aside class="control-sidebar control-sidebar-dark">
		<!-- Create the tabs -->
		<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
			<li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
			<li class="active"><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
		</ul>

		<!-- Tab panes -->
		<div class="tab-content">
			<!-- Home tab content -->
			<div class="tab-pane" id="control-sidebar-home-tab">
				<!-- /.control-sidebar-menu -->
				<!-- /.control-sidebar-menu -->
        	</div>
        	<!-- /.tab-pane -->
        	<!-- Settings tab content -->
        	<div class="tab-pane active" id="control-sidebar-settings-tab">
        		<h3 class="control-sidebar-heading" ><a style="color: white" href="<?php echo base_url() ?>/ControlPanel/menu">Panel de Control</a></h3>
        		<div class="form-group">
        			<label class="control-sidebar-subheading">
        				<a style="color: white" href="<?php echo base_url() ?>/ControlPanel/menu">Menú</a>
        			</label>
        			<p>Configuración de perfil, roles, usuario, departamentos, personal, etc.</p>
        		</div>
        		<div class="form-group">
        			<h3 class="control-sidebar-heading">Acceso Rápido</h3>
        			<label class="control-sidebar-subheading">Añadir un usario</label>
        			<label class="control-sidebar-subheading">Cambio de contraseña</label>
        			<p>Activate the boxed layout</p>
        		</div>
			<!-- /.tab-pane -->
		</div>
	</aside>


	<!-- /.control-sidebar -->
	<!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
	<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->