<body class="hold-transition skin-green sidebar-mini">
	<div class="wrapper">
		<header class="main-header">
			<!-- Logo -->
			<a href="<?php echo base_url() ?>" class="logo">
				<!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><b>🅱</b>u</span>
				<!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><b>🅱urr</b>itos</span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- Messages: style can be found in dropdown.less-->
						<li class="dropdown messages-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-envelope-o"></i>
								<!-- In case of alerrts -->
								<!-- <span class="label label-success">0</span> -->
							</a>
							<ul class="dropdown-menu"> 
							</ul>
						</li>

						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="<?php echo base_url(); ?>img/happy_negro.png" class="user-image" alt="User Image">
								<span class="hidden-xs">Santino Lee</span>
							</a>
							<ul class="dropdown-menu">
								<!-- User image -->
								<li class="user-header">
									<img src="<?php echo base_url(); ?>/img/happy_negro.png" class="img-circle" alt="User Image">
									<p>
										Santino Lee
									</p>
								</li>
								<!-- Menu Footer-->
								<li class="user-footer">
									<div class="pull-left">
										<a href="#" class="btn btn-default btn-flat">Profile</a>
									</div>
									<div class="pull-right">
										<a href="#" class="btn btn-default btn-flat">Sign out</a>
									</div>
								</li>
							</ul>
						</li>
						<!-- Control Sidebar Toggle Button -->
						<li>
							<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
						</li>
					</ul>
				</div>
			</nav>
		</header>

		
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" style="margin-left: 0">
			<!-- Main content -->
			<section class="content">
				<section class="content-header">
					<h1>
						<?php echo $title ?>
						<small><?php echo $subtitle ?></small>
					</h1>
					<ol class="breadcrumb">
						<li><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i> Inicio</a></li>
						<?php /*
						Cycle to iterate trought which pages you are.
						Information for this is fed from the controller.
						*/ ?>
						<?php if (is_array($navnodes)): ?>
							<?php foreach ($navnodes as $key => $value): ?>	
						<li class=""><?php echo $value ?></li>
							<?php endforeach ?>
						<?php endif ?>
						<?php if (!is_array($navnodes)): ?>
							<?php if (strcmp($title, "Inicio")): ?>	
							<li class="active"><?php echo $title ?></li>
							<?php endif ?>
						<?php endif ?>
					</ol>
				</section>
				<section class="content">