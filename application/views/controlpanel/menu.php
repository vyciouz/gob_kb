<?php
$options = [
	"users" => [
		"name" => "Usuarios", "image" => "img/user_m.png", "url" => base_url()."users/list"
	],
	"roles" => [
		"name" => "Roles y permisos", "image" => "img/roles.png", "url" => "#"
	],
	"departments" => [
		"name" => "Departamentos", "image" => "img/department.png", "url" => "#"
	],
	"subjects" => [
		"name" => "Temas", "image" => "img/subjects.png", "url" => "#"
	],
	"documents" => [
		"name" => "Documentos", "image" => "img/subjects.png", "url" => "#"
	],
	"links" => [
		"name" => "Enlaces", "image" => "img/subjects.png", "url" => "#"
	],
];

?>

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Opciones</h3>
        <div class="box-tools pull-right">
            <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button> -->        
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
    	<div class="row">
    		<?php foreach ($options as $key => $value): ?>
	    	<div class="col-md-3" style="text-align: center;">
	    		<div style="margin: auto;">
    				<a href="<?php echo $value['url'] ?>">
	    				<img style="width: 150px; height: 150px;" src="<?php echo base_url().$value['image'] ?>">
    				</a>
	    		</div>
	    		<div>
	    			<a href="<?php echo $value['url'] ?>">
	    			<?php echo $value['name'] ?>
	    			</a>
	    		</div>
	    	</div>
    		<?php endforeach ?>
    	</div>
    </div>
    <!-- /.box-body -->
	
    <div class="box-footer">
    	Control de usuarios.
    </div>
</div>