<?php
class ControlPanel extends CI_Controller{


	// Main screen, here every option available will be shown
	public function menu($page = 'menu'){

		$data['title'] = "Panel de Control";
		$data['subtitle'] = "Menú principal";
		$data['navnodes'] = ["Panel de Control"];
		$data['css'] = ["css/control_panel.css"];
		$data['js'] = [""];

		$this->load->view('templates/header',$data);
		$this->load->view('templates/navbar_only',$data);
		$this->load->view('controlpanel/'.$page);
		$this->load->view('templates/scripts');
		$this->load->view('templates/footer_no_sidebar');
	}

}
?>