<?php
class Users extends CI_Controller{
	// Not being used

	function _remap($method,$args){
    	if (method_exists($this, $method)){
    		$this->$method($args);
    	}else{
    		$this->index($method,$args);
    	}
    }

	public function profile(){
		$this->load->view('templates/header');
		$data['departments'] = $this->departments_model->get_departments();
		
		$this->load->view('templates/navbar_and_sidebar',$data);
		$this->load->view('users/profile');
		$this->load->view('templates/footer');	
	}

	public function login(){
		# Page information
		$data['title'] = "Iniciar Sesión";
		$data['subtitle'] = "Menú principal";
		$data['navnodes'] = ["Panel de Control"];
		# /Page information
		$this->load->view('templates/header',$data);
		$this->load->view('users/login');

	}

	public function new(){

		$this->load->helper('form');
		# Page information
		$data['title'] = "Nuevo Usuario";
		$data['subtitle'] = "Usuarios";
		$data['navnodes'] = ['Panel de Control',"Usuarios","Nuevo Usuario"];
		# /Page information
		$data['css'] = array("bower_components/select2/dist/css/select2.min.css");
		$this->load->view('templates/header',$data);
		$data['departments'] = $this->departments_model->get_departments();
		$this->load->view('templates/navbar_only',$data);
		$this->load->view('users/new');
		$this->load->view('templates/scripts');
		$this->load->view('templates/footer_no_sidebar');
	}

	public function result(){
		$data['usernanme'] = $_POST['usernanme'];
		$this->load->view('users/result',$data);
	}

	public function list(){
		# Data load
		$data['users'] = $this->users_model->get_users();
		# /Data load

		$data['title'] = "Lista de Usuarios";
		$data['subtitle'] = "usuarios";
		$data['navnodes'] = ["Panel de Control", "Usuarios","Lista de Usuarios"];
		$this->load->view('templates/header',$data);
		$this->load->view('templates/navbar_only',$data);
		$this->load->view('users/list',$data);
		$this->load->view('templates/scripts');
		$this->load->view('templates/footer_no_sidebar');

	}
}
?>