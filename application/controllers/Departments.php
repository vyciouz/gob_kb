<?php
class Departments extends CI_Controller{
	// Not being used

	function _remap($method,$args){
    	if (method_exists($this, $method)){
    		$this->$method($args);
    	}else{
    		$this->index($method,$args);
    	}
    }

    public function index(){
		$this->load->view('templates/header');
		$data['departments'] = $this->departments_model->get_departments();
		$data['args'] = $dept;
		$this->load->view('templates/navbar_and_sidebar',$data);
		$this->load->view('departments/index');
		$this->load->view('templates/footer');
	}

	public function orgchart($dept){
		# Data load
		# Loads departments info to show it in the sidebar menu
		$data['departments'] = $this->departments_model->get_departments();
		# Get department personnel
		$data['personnel'] = $this->orgchart_model->get_personnel($dept[0]);
		#/ Data load
		# Page information
		$data['title'] = "Organigrama";
		$data['subtitle'] = "Nombre del Departamento";
		$data['navnodes'] = ["Nombre del departamento","Información General"];
		//Extra CSS needed
		// $data['css'] = [""];
		//Extra JS needed
		// $data['js'] = [""];


		$this->load->view('templates/header',$data);
		$this->load->view('templates/navbar_and_sidebar',$data);
		$this->load->view('departments/orgchart');
		$this->load->view('templates/scripts');
		$this->load->view('orgchart/orgchart_script');
		$this->load->view('templates/footer');
	}
	
	public function info($args){
		$id = $args[0];
		# Data load
		# Loads departments info to show it in the sidebar menu
		$data['info'] = $this->departments_model->get_department_info($id);
		# /Data load
		# Page information
		$data['title'] = "Información General";
		$data['subtitle'] = "Nombre del Departamento";
		$data['navnodes'] = ["Nombre del departamento","Información General"];
		# /Page information
		$this->load->view('templates/header',$data);
		$data['departments'] = $this->departments_model->get_departments();
		$this->load->view('templates/navbar_and_sidebar',$data);
		$this->load->view('departments/info');
		$this->load->view('templates/scripts');
		$this->load->view('templates/footer');
	}

	public function articles($dept){
		# Data load
		$data['departments'] = $this->departments_model->get_departments();
		# /Data load
		# Page information
		$data['title'] = "Temas";
		$data['subtitle'] = "Nombre del Departamento";
		$data['navnodes'] = ["Nombre del departamento","Temas"];
		# /Page information
		$this->load->view('templates/header',$data);
		$data['args'] = $dept;
		$this->load->view('templates/navbar_and_sidebar',$data);
		$this->load->view('departments/articles');
		$this->load->view('templates/scripts');
		$this->load->view('templates/footer');
	}

	public function newarticle($dept){
		
		# Data load
		$data['departments'] = $this->departments_model->get_departments();
		# /Data load
		# Page information
		$data['title'] = "Temas";
		$data['subtitle'] = "Nombre del Departamento";
		$data['navnodes'] = ["Nombre del departamento","Temas"];
		# /Page information
		$this->load->view('templates/header',$data);
		$data['args'] = $dept;
		$this->load->view('templates/navbar_and_sidebar',$data);
		$this->load->view('departments/newarticle');
		$this->load->view('templates/scripts');
		$this->load->view('templates/footer');
	}

	// Main screen, here every option available will be shown
	public function links(){
		$this->load->view('templates/header');
		$this->load->view('templates/navbar_only');
		
		$this->load->view('templates/footer_no_sidebar');
	}

}
?>