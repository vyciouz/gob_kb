<?php
class Pages extends CI_Controller{
	public function view($page = 'start'){
		if (!file_exists(APPPATH.'views/pages/'.$page.'.php')) {
			show_404();
		}

		switch ($page) {
			case 'start':
				$data['title'] = "Inicio";
				$data['subtitle'] = "Bienvenido";
				$data['navnodes'] = "";
				break;

			case 'about':
				$data['title'] = "Acerca de";
				$data['subtitle'] = "Base de conocimiento";
				$data['navnodes'] = "";
				break;

			case 'faqs':
				$data['title'] = "Preguntas Frecuentes";
				$data['subtitle'] = "Base de conocimiento";
				$data['faqs'] = $this->faqs_model->get_faqs();
				$data['navnodes'] = "";
				break;
			
			default:
				$data['title'] = "Nothing here";
				$data['subtitle'] = "Nothing here";
				$data['navnodes'] = "?";
				break;
		}
		


		$this->load->view('templates/header',$data);
		$data['departments'] = $this->departments_model->get_departments();
		
		$this->load->view('templates/navbar_and_sidebar',$data);
		$this->load->view('pages/'.$page, $data);
		$this->load->view('templates/scripts');
		$this->load->view('templates/footer');
	}
}
?>