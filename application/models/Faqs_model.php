<?php
class Faqs_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	// methods always inside the class
	// remember to add model to autoload
	public function get_faqs(){
	    $query = $this->db->get('FAQS');
	    $result = $query->result_array();
	    return $result;
	}
}
?>