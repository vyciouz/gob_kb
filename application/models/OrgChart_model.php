<?php
class OrgChart_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	// methods always inside the class
	// remember to add model to autoload
	public function get_personnel($id){
	    $query = $this->db->query("SELECT * FROM t_personnel WHERE ID_DEPT = $id;");
	    $result = $query->result_array();
	    return $result;
	}

	public function get_department_info($id){
		$query = $this->db->query("SELECT * FROM cat_departments WHERE ID_DEPT = $id");
		$result = $query->result_array();
	    return $result;
	}

}
?>