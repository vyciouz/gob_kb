<?php
class Users_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	// methods always inside the class
	// remember to add model to autoload
	public function get_users(){
	    $query = $this->db->get('table_users');
	    $result = $query->result_array();
	    return $result;
	}
}
?>