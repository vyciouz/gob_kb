<?php
// if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  
// require_once dirname(__FILE__) . '/tcpdf/tcpdf.php';
  
// class Pdf extends TCPDF
// {
//  function __construct()
//  {
//  parent::__construct();
//  }
// }
  
/* End of file Pdf.php */
/* Location: ./application/libraries/Pdf.php */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    // Incluimos el archivo fpdf
    require_once APPPATH."/third_party/fpdf/fpdf.php";
 
    //Extendemos la clase Pdf de la clase fpdf para que herede todas sus variables y funciones
class Pdf extends FPDF {


    function __construct ($orientation = 'P', $unit = 'pt', $format = 'Letter', $margin = 40) { 
        parent::__construct($orientation, $unit, $format, $margin); //change default('P', 'mm', 'A4')     
        
    }  
    function Header() { 
        // $path = APPPATH."libraries/logo.png";
        // $this->Image($path);
        $this->SetFont('Helvetica', 'B', 20); 
        $this->SetFillColor(255, 255, 255); 
        $this->SetTextColor(0); 
    }


    function Footer() { 
        $this->SetFont('Arial', '', 12); 
        $this->SetTextColor(0); 
        $this->SetXY(0,-60); 
    }


}

?>